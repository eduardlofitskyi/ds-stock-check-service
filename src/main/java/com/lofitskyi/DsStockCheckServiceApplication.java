package com.lofitskyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class DsStockCheckServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(DsStockCheckServiceApplication.class, args);
	}
}
